# Electron global shortcut order bug

Run: `npm start`
Press: `Super+Shift+Esc`
Then press: `Shift+Super+Esc`

On Ubuntu 18.04 pressing `Shift+Super+Esc` doesn't work the first time, but works after that.
On Ubuntu 20.04 pressing `Shift+Super+Esc` always works.
