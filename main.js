var {app, globalShortcut} = require('electron');

app.on('ready', function () {
    globalShortcut.register('Super+Shift+Esc', function () {
        console.log('Super+Shift+Esc pressed');
    });
});
